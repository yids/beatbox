#include <cstdlib>
#include <signal.h>
#include "MidiFile.h"
#include "Options.h"
#include <iostream>
#include "RtMidi.h"
#include <stdio.h>
using namespace std;

#define NUM_TRACKS 16
//#define NUM_STEPS 96 // 1 bar
#define NUM_STEPS 384
int step = 0;
MidiFile midifile;
MidiEvent currentEvent;
RtMidi::Api api = RtMidi::Api::UNSPECIFIED;
RtMidiOut *midiout0 = new RtMidiOut(api, "mmp-drum");
RtMidiOut *midiout1 = new RtMidiOut(api, "mmp-synth");
RtMidiOut *sysexout = new RtMidiOut(api, "sysex-out");
RtMidiIn *sysexin = new RtMidiIn(api, "sysex-in");


int track = 1;
bool tracks_playing[NUM_TRACKS];


enum sysMsgType { NATIVE_START,
                  NATIVE_STOP,
                  SET_SYS_MIDI,
                  ALL_LEDS_ON,
                  ALL_LEDS_OFF,
                  LED_ON,
                  LED_OFF,
                  LED_BLINK,
                  LED_FLASH};

namespace sysMsg
{
  unsigned char nativeStart[4] =  {0 ,0, 1,  247};
  unsigned char setSysMidi[45] =  {63, 42, 0, 0, 5, 9, 9, 16, 17, 127,
                                   127, 3, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 1,
                                   2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 247};
  unsigned char ledsOn[13]     = {63, 10, 1, 127, 127, 127, 127, 127, 3, 83, 69, 89, 247};
  unsigned char ledsOff[13]    = {63, 10, 1, 0, 0, 0, 0, 0, 3, 83, 69, 89, 247};
  unsigned char nativeStop[4]  = {0 ,0, 0,  247};
  unsigned char ledOn[4]       = {1, 0, 32, 247}; // element 1 is pad number                                                                                                         
  unsigned char ledOff[4]      = {1, 0, 0,  247}; // element 1 is pad number 
  unsigned char ledFlash[4]    = {1, 0, 68, 247};  // element 1 is pad number 
  unsigned char ledBlink[4]    = {1, 0, 99, 247};
}

void printMsg(std::vector<unsigned char> message)
{
  std::cout<<"sysex msg: ";
  for (auto i: message)
    std::cout << int(i) << ' ';
  std::cout<<"\n";
}

int sysexSend(sysMsgType msgType, int value)
{
  unsigned char sysFirstBytes[5] = {240, 66, 64, 110, 8}; // first part of sysex msg
  std::vector<unsigned char> message(sysFirstBytes, sysFirstBytes+sizeof(sysFirstBytes));

  switch(msgType){
    case NATIVE_START:
      std::cout<<"start native mode\n";
      for(int i=0; i < int(sizeof(sysMsg::nativeStart)); i++)
        message.push_back(sysMsg::nativeStart[i]);
      break;
    case NATIVE_STOP:
      std::cout<<"stop native mode\n";
      for(int i=0; i < int(sizeof(sysMsg::nativeStop)); i++)
        message.push_back(sysMsg::nativeStop[i]);
      break;
    case SET_SYS_MIDI:
      for(int i=0; i < int(sizeof(sysMsg::setSysMidi)); i++)
        message.push_back(sysMsg::setSysMidi[i]);
      break;
    case ALL_LEDS_ON:
      for(int i=0; i < int(sizeof(sysMsg::ledsOn)); i++)
        message.push_back(sysMsg::ledsOn[i]);
      break;
    case ALL_LEDS_OFF:
      for(int i=0; i < int(sizeof(sysMsg::ledsOff)); i++)
        message.push_back(sysMsg::ledsOff[i]);
      break;
    case LED_ON:
       for(int i=0; i < int(sizeof(sysMsg::ledOn)); i++)
         message.push_back(sysMsg::ledOn[i]);
       message[6]=value;
      break;
    case LED_OFF:
      for(int i=0; i < int(sizeof(sysMsg::ledOff)); i++)
        message.push_back(sysMsg::ledOff[i]);
      message[6]=value;

      break;
    case LED_BLINK:
      for(int i=0; i < int(sizeof(sysMsg::ledBlink)); i++)
        message.push_back(sysMsg::ledBlink[i]);
      message[6]=value;
      break;
    case LED_FLASH:
      for(int i=0; i < int(sizeof(sysMsg::ledFlash)); i++)
        message.push_back(sysMsg::ledFlash[i]);
        message[6]=value;
      break;
  }
#ifdef DEBUG_SYSEX
  printMsg(message);
#endif                                                                                                                                                                               
  sysexout->sendMessage(&message); 
  return 0;
}


void loadfile(char *filename)
{
  midifile = NULL;
  midifile.read(filename);
  if (!midifile.status()) {
     printf("Error reading MIDI file %s\n",filename);
     exit(1);
  }
  else
    printf("loaded file %s succesfully\n", filename);
}

void playnote(int step)
{
  MidiEvent* mev;
  int deltatick;
  for(int i=0; i<midifile.getTrackCount() ; i++){
    if(tracks_playing[i] == true && midifile[i].getSize() > 0){
      for (int event=0; event < midifile[i].size(); event++) {
        mev = &midifile[i][event];
        if (event == 0) {
           deltatick = mev->tick;
        } else {
           deltatick = mev->tick - midifile[i][event-1].tick;
        }
        if(mev->tick == step){
  	      if(midifile[i][event].isNoteOn()){
	        std::vector<unsigned char> message;
	  	    message.push_back (144); // type noteon
            message.push_back ((int)(*mev)[1]); // note
            message.push_back (90); // velo
//		    printf("playnot: %u\n", (int)(*mev)[1]);
			if(midifile[i][event].getChannelNibble()==0)
   		      midiout0->sendMessage( &message ); 
 			if(midifile[i][event].getChannelNibble()==1)
  			  midiout1->sendMessage( &message ); 
		  }
	    }
	  }
    }
  }
}


void mycallback( double deltatime, std::vector< unsigned char > *message, void *userData )
{
//  printf("message: %u\n",(int)message->at(0));
  if((int)message->at(0) == 248){
    playnote(step*8);
    step++;
	if(step == NUM_STEPS)
    step = 0;
  }
  if((int)message->at(0) == 144){
    tracks_playing[(int)message->at(1)] = !tracks_playing[(int)message->at(1)];
    printf("track %u is now %d\n",(int)message->at(1), tracks_playing[(int)message->at(1)]);
  }
  if((int)message->at(0) == 192){
  char filename[16];
  snprintf(filename, 16, "%u.midi",(int)message->at(1));
  printf("loading file: %s\n", filename);
  loadfile(filename);
  }

}

void sysexInCb( double deltatime, std::vector< unsigned char > *message, void * /*userData*/ )                                                                                       
{
 unsigned int nBytes = message->size();
/*  for ( unsigned int i=0; i<nBytes; i++ )
    std::cout << "Byte " << i << " = " << (int)message->at(i) << ", ";
  if ( nBytes > 0 )
    std::cout << "stamp = " << deltatime << std::endl;
*/
  if(message->at(0) == 240){
//   if(message->at(6) == 14)
//     loopClear();
   if(message->at(6) > 15){
     int buttnum =  message->at(6)-63;
     printf("button: %u\n", buttnum);
     tracks_playing[buttnum-1] = !tracks_playing[buttnum-1];
     printf("track %u is now %d\n",buttnum-1, tracks_playing[buttnum-1]);
     if(tracks_playing[buttnum-1] == true)
       sysexSend(LED_ON, buttnum-1);
     if(tracks_playing[buttnum-1] == false)
       sysexSend(LED_OFF, buttnum-1);

//     printf("velo %u for step %u\n", seq->noteVelocities[seq->selectedStep], seq->selectedStep);
   }
  } 

}

int main(int argc, char** argv) 
{


  RtMidiIn *midiin = new RtMidiIn(api,"mmp-in");
 
  // Check available ports.
  unsigned int nPorts = midiin->getPortCount();
  if ( nPorts == 0 ) {
    std::cout << "No ports available!\n";
	return 1;
  }
  midiin->openPort( 0, "mmp-in" );
  midiout0->openPort( 0, "mmp-drum");
  midiout1->openPort( 0, "mmp-synth");
  sysexout->openPort(0, "sysex-out");
  sysexin->openPort(0, "sysex-in");
  sysexin->setCallback( &sysexInCb );
  sysexin->ignoreTypes( false, true, true );

  midiin->setCallback( &mycallback );
  midiin->ignoreTypes( false, false, false );

//  midifile.joinTracks();
  loadfile("0.midi");
  midifile.absoluteTicks();
  for(int i=0; i < NUM_TRACKS; i++){
    tracks_playing[i] = false;
  }
  printf("sizeof tings: %u\n", midifile.getTrackCount());

  std::cout << "\nconnect sysex to pk and press enter\n";
  char input;
  std::cin.get(input);

  sysexSend(NATIVE_START, 0);
  sysexSend(SET_SYS_MIDI, 0);
  sysexSend(ALL_LEDS_ON, 0);
  sysexSend(ALL_LEDS_OFF, 0);
  sysexSend(LED_ON, 5);

  std::cout << "\nReading MIDI input ... press <enter> to quit.\n";

  std::cin.get(input);
  return 0; 
}   



