CPP=g++
MIDIFILEDIR = midifile
INCDIR      = $(MIDIFILEDIR)/include
OBJDIR      = $(MIDIFILEDIR)/obj
LIBDIR      = $(MIDIFILEDIR)/lib
LIBFILE		= midifile
LIBPATH     = $(LIBDIR)/lib$(LIBFILE).a
PREFLAGS    = -O3 -Wall -std=c++11 -D__LINUX_ALSA__ -lasound -lpthread -lrtmidi -I$(INCDIR)    
POSTFLAGS  ?= -L$(LIBDIR) -l$(LIBFILE)

mmp: mmp.cpp
	$(CPP) $(PREFLAGS) $(LIBPATH) mmp.cpp -o mmp $(POSTFLAGS)

