#!/bin/bash 

trap ctrl_c INT

ctrl_c()
{
  printf "cought control c, killing everything\n"
  killall drumkv1_jack
  killall yoshimi
}


drumkv1_jack -g & 
yoshimi -i  

